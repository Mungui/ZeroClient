#ifndef ZERO_CLIENT_RENDER_SYSTEM_H
#define ZERO_CLIENT_RENDER_SYSTEM_H

void initializeRenderer(void);

void destroyRenderer(void);

#endif //ZERO_CLIENT_RENDER_SYSTEM_H
