#ifndef ZERO_CLIENT_NETWORK_SYSTEM_H
#define ZERO_CLIENT_NETWORK_SYSTEM_H

void startDaemon(void);

void stopDaemon(void);

#endif //ZERO_CLIENT_NETWORK_SYSTEM_H
